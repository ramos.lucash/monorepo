module.exports = {
  "*.{ts,tsx}": [
    "eslint --max-warnings 0 --cache --fix",
    "prettier --write"
  ]
}