/* eslint-disable @typescript-eslint/no-var-requires */
const eslint = require('./.eslintrc');
const prettier = require('./.prettierrc');

module.exports = {
  eslint,
  prettier,
};

/* eslint-enable @typescript-eslint/no-var-requires */
