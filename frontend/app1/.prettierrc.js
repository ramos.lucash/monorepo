/* eslint-disable @typescript-eslint/no-var-requires */
const { prettier } = require('@monorepo/lint-config');

module.exports = prettier;
/* eslint-enable @typescript-eslint/no-var-requires */
