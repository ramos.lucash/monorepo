/* eslint-disable @typescript-eslint/no-var-requires */
const { eslint } = require('@monorepo/lint-config');

module.exports = eslint;
/* eslint-enable @typescript-eslint/no-var-requires */
