/* eslint-disable @typescript-eslint/no-var-requires */
const lintStaged = require('@monorepo/precommit-config');

module.exports = lintStaged;
/* eslint-enable @typescript-eslint/no-var-requires */
